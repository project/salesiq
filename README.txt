### INSTALLING
 
1. Extract zohosalesiq tarball into your modules directory so it looks like modules/salesiq
2. Navigate to Administer -> Extend -> Install new module and enable the module.
3. Navigate to Administer -> Configuration -> Zoho SalesIQ and add your widget code as well as any other configuration options you want.
 
### CREDITS